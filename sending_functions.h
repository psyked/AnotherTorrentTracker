#ifndef SENDING_FUNCTION_H
#define SENDING_FUNCTION_H

#include "tracker4.h"

void sendData(int, const char*, size_t);
void sendString(int, const char*);
void sendWebPage(int, int, int, int);

#endif