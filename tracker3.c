#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/sendfile.h>
#include <sys/stat.h>
#include <arpa/inet.h>
#include <sys/epoll.h>

#include "utility.h"
#include "inet_socket.h"

#define BUFFER_SIZE 1024
#define EPOLL_SIZE 10
#define MAX_EVENTS 10

int totalClients = 0;
int currentClients = 0;

ssize_t readLine(int fd, void *buffer, size_t n)
{
    ssize_t numRead; /* # of bytes fetched by last read() */
    size_t totRead;  /* Total bytes read so far */
    char *buf;
    char ch;

    if (n <= 0 || buffer == NULL)
    {
        errno = EINVAL;
        return -1;
    }

    buf = (char *)buffer; /* No pointer arithmetic on "void *" */

    totRead = 0;
    for (;;)
    {
        numRead = read(fd, &ch, 1);

        if (numRead == -1)
        {
            if (errno == EINTR) /* Interrupted --> restart read() */
                continue;
            else
                return -1; /* Some other error */
        }
        else if (numRead == 0)
        {                     /* EOF */
            if (totRead == 0) /* No bytes read; return 0 */
                return 0;
            else /* Some bytes read; add '\0' */
                break;
        }
        else
        { /* 'numRead' must be 1 if we get here */
            if (totRead < n - 1)
            { /* Discard > (n - 1) bytes */
                totRead++;
                *buf++ = ch;
            }

            if (ch == '\n')
                break;
        }
    }

    *buf = '\0';
    return totRead;
}

void send_data(int fd, const char *buf, size_t len)
{
    ssize_t n;
    size_t pos = 0;
    const char *ptr = buf;

    while (len > pos)
    {
        switch (n = write(fd, ptr + pos, len - pos))
        {
        case 0:
            _exit(0);
        /* NOT REACHED */
        case -1:
            if (errno != EINTR && errno != EAGAIN)
                perror("send_data");
            break;
        default:
            pos += n;
        }
    }
}
void send_string(int fd, const char *msg)
{
    send_data(fd, msg, strlen(msg));
}

void send_page(int cfd)
{
    send_string(cfd, "HTTP/1.1 200 OK\r\n");
    send_string(cfd, "Server: TrackerServer\r\n\r\n");
    send_string(cfd, "<!DOCTYPE html"
                     "PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN'"
                     "'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'>"
                     "<html xmlns='http://www.w3.org/1999/xhtml'>"
                     "<head>"
                     "<title>Tracker information</title>"
                     "</head>"
                     "<body>"
                     "Opened Connexions : ");
    char buff[10];
    sprintf(buff, "%d", currentClients);
    send_string(cfd, buff);
    send_string(cfd, " Connexions from the beginning of the server : ");
    sprintf(buff, "%d", totalClients);
    send_string(cfd, buff); 
    send_string(cfd, "</body>"
                     "</html>");
}

static void
handleRequest(int cfd)
{
    char buf[BUFFER_SIZE];
    char filename[BUFFER_SIZE] = {0};
    int numRead;
    while ((numRead = readLine(cfd, buf, BUFFER_SIZE)) > 0)
    {
        if (strcmp(buf, "\r\n") == 0)
            break;

        if (strstr(buf, "GET"))
        {
            char *ptr = strstr(buf, " HTTP/");
            *ptr = '\0';
            strcpy(filename, buf + 4);
        }
    }

    if (strlen(filename) > 0)
    {
        if (strcmp(filename, "/index.html") == 0 || strcmp(filename, "/") == 0)
        {
            send_page(cfd);
        }
        else
        {
            char *strings[3];
            strings[0] = "<html><head><title>404 Not Found</title></head>"
                "<body>The page ";
            strings[1] = filename;
            strings[2] = " doesn't exist, try /index.html</body></html>";
            send_string(cfd, "HTTP/1.1 404 NOT FOUND\r\n");
            send_string(cfd, "Server: TrackerServer\r\n\r\n");
            send_string(cfd, strings[0]);
            send_string(cfd, strings[1]);
            send_string(cfd, strings[2]);
        }
    }
    else
    {
        send_string(cfd, "<html><head><title>Bad request</title></head>");
        send_string(cfd, "<body>Bad filename requested, try /index.html</body></html>");
    }
    printf("[%s] exit\n", filename);
    send_string(cfd, "\r\n");
    
    // close the fd to let the client display the page
    close(cfd);
    printf("Closed connection from server\n");
    currentClients--;

    if (numRead == -1)
        errExit("Error from read()");
}

int main()
{
    int listen_fd = inetListen("8080", 5, NULL);
    if (listen_fd == -1)
    {
        errExit("inetListen");
    }

    // Initialize the epoll instance
    int epfd = epoll_create(EPOLL_SIZE);
    if (epfd == -1)
    {
        errExit("epoll_create");
    }
    // struct epoll_event {
    //     uint32_t     events;      /* Epoll events */
    //     epoll_data_t data;        /* User data variable */
    // };

    // event : data can be read
    struct epoll_event ev = {.events = EPOLLIN};

    // typedef union epoll_data {
    //     void        *ptr;
    //     int          fd;
    //     uint32_t     u32;
    //     uint64_t     u64;
    // } epoll_data_t;

    // add listen_fd to the interest list
    ev.data.fd = listen_fd;
    if (epoll_ctl(epfd, EPOLL_CTL_ADD, listen_fd, &ev) == -1)
    {
        errExit("epoll_ctl");
    }
    // add STDIN to the interest list
    ev.data.fd = STDIN_FILENO;
    if (epoll_ctl(epfd, EPOLL_CTL_ADD, STDIN_FILENO, &ev) == -1)
    {
        errExit("epoll_ctl");
    }

    char buffer[BUFFER_SIZE];
    // the event list used with epoll_wait()
    struct epoll_event evlist[MAX_EVENTS];

    char x = 0;
    while (x != 'q')
    {
        int nb_fd_ready = epoll_wait(epfd, evlist, MAX_EVENTS, -1);

        if (nb_fd_ready == -1)
        {
            if (errno == EINTR) // restart if interrupted by signal
                continue;
            else
                errExit("epoll");
        }

        for (int i = 0; i < nb_fd_ready; ++i)
        {
            int fd = evlist[i].data.fd;
            if (fd == STDIN_FILENO)
            {
                if ((read(STDIN_FILENO, &x, 1) == 1) && (x == 'q'))
                {
                    break;
                }
            }
            else if (fd == listen_fd)
            {
                struct sockaddr_in client_addr;
                char client_addr_str[INET6_ADDRSTRLEN];
                socklen_t client_len = sizeof(struct sockaddr_in);
                int client_fd = accept(listen_fd, (struct sockaddr *)&client_addr, &client_len);
                inetAddressStr((struct sockaddr *)&client_addr, client_len,
                               client_addr_str, INET6_ADDRSTRLEN);
                if (client_fd == -1 && errno != EINTR)
                {
                    errExit("accept");
                }
                // There is a new connection
                printf("Accept a new connection from %s\n", client_addr_str);
                totalClients++;
                currentClients++;
                ev.data.fd = client_fd;
                if (epoll_ctl(epfd, EPOLL_CTL_ADD, client_fd, &ev) == -1)
                {
                    close(client_fd);
                }
                // sent the web page
                handleRequest(client_fd);
            }
            else
            { // a client fd is ready
                int num_read = read(fd, buffer, BUFFER_SIZE);

                if (num_read < 0 && errno != EINTR)
                {
                    errExit("read");
                }
                else if (num_read == 0)
                { // The client closed the connection
                    printf("Closed connection from a client\n");
                    currentClients--;
                    close(fd);
                    continue;
                }
                // get data from client
                buffer[num_read] = 0; // end of string
                //treat data from buffer but we don't need it
            }
        }
    }

    close(listen_fd);
    return EXIT_SUCCESS;
}
