#include "inet_socket.c"
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <sys/epoll.h>
#include <stdlib.h>
#include "utility.c"
#include "read_line.c"

#define BACKLOG 5 

#define EPOLL_SIZE 10
#define MAX_EVENTS 10
#define BUFFER_SIZE 100

int make_non_block(int fd) 
{
    return fcntl(fd, F_SETFL, O_NONBLOCK);
}

void send_data(int fd, const char *buf, size_t len) 
{
    ssize_t n;
    size_t pos = 0;
    const char *ptr = buf;

    while (len > pos)
    {
        switch (n = write(fd, ptr + pos, len - pos))
        {
        case 0:
            _exit(0);
        /* NOTREACHED */
        case -1:
            if (errno != EINTR && errno != EAGAIN)
                perror("send_data");
            break;
        default:
            pos += n;
        }
    }
}
void send_string(int fd, const char *msg)
{
    send_data(fd, msg, strlen(msg));
}

static void
handleRequest(int client_fd)
{
    char buf[BUFFER_SIZE];
    int numRead;
    while ((numRead = readLine(client_fd, buf, BUFFER_SIZE)) > 0)
    {
        printf("[%d] %s", numRead, buf);
        if (strcmp(buf, "\r\n") == 0)
            break;
    }
    send_string(client_fd, "\r\n");
    // close the fd to let the client display the page
    close(client_fd);

    if (numRead == -1)
        errExit("Error from read()");
}

int main() {
	int server = inetListen("55555", BACKLOG, NULL);
	if(server == -1){
		errExit("inetListen");
	}
	
	// Initialize the epoll instance
	int epfd;
	epfd = epoll_create(EPOLL_SIZE);
	if (epfd == -1)
        errExit("epoll_create");

    struct epoll_event ev;
    ev.events = EPOLLIN;	// event : data can be read

    // add listen_fd (server) to the interest list
    ev.data.fd = server;
    if (epoll_ctl(epfd, EPOLL_CTL_ADD, server, &ev) == -1)
        errExit("epoll_ctl");

    // add STDIN to the interest list
    ev.data.fd = STDIN_FILENO;
    if (epoll_ctl(epfd, EPOLL_CTL_ADD, STDIN_FILENO, &ev) == -1)
        errExit("epoll_ctl");

    // the event list used with epoll_wait()
    struct epoll_event evlist[MAX_EVENTS];

    char buffer[BUFFER_SIZE + 1];
    char x = 0;
    while (x != 'q') {
        int nb_fd_ready = epoll_wait(epfd, evlist, MAX_EVENTS, -1);

        // if > MAX_EVENTS events are available, then succesive calls to
        // epoll_wait fill evlist in a round robin manner

        if (nb_fd_ready == -1) {
            if (errno == EINTR) // restart if interrupted by signal
                continue;
            else
                errExit("epoll");
        }

        for (int i = 0 ; i < nb_fd_ready ; ++i) {
            int fd = evlist[i].data.fd;
            if (fd == STDIN_FILENO) {
                // Close the server by typing the letter q
                if ((read(STDIN_FILENO, &x, 1) == 1) && (x == 'q'))
                    break;
            } else if (fd == server) {
                int client_fd = accept(server, NULL, NULL);

                // The accept system call returns an error
                // this error should be handled more gracefully
                if (client_fd == -1 && errno != EINTR)
                    errExit("accept");
 
                // There is a new connection
                if (client_fd != -1) {
                    printf("Accept a new connection...\n");
                    ev.data.fd = client_fd;
                    if (epoll_ctl(epfd, EPOLL_CTL_ADD, client_fd, &ev) == -1)
                        close(client_fd);

                    handleRequest(client_fd);
                }
            } else { // a client fd is ready
                int nread = read(fd, buffer, BUFFER_SIZE);

                if(nread < 0 && errno != EINTR){
                	errExit("read");
                }
                else if(nread == 0){	//client leave
                	printf("Connection close.");
                	close(fd);
                	continue;
                }
                else{
                	printf("(%d)\n", fd);
                }
            }
        }
    }

    close(server);
    return EXIT_SUCCESS;
}