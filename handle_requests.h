#ifndef HANDLER_REQUEST_H
#define HANDLER_REQUEST_H

#include "tracker4.h"
#include "read_line.h"
#include "sending_functions.h"

struct infoTorrent
{
    char *hash, *id, *event, *ip;
    int port, key, uploaded, downloaded, left, compact;
};
struct peer
{
    char *id, *ip;
    int port;
};

void sendInfoTorrent(int, struct peer*);
void addToList(struct infoTorrent, struct peer*);
struct infoTorrent parseInfoTorrent(char*, int);
void handleRequestHTML(int, int, int, int);
void handleRequestTorrent(int, char*, int*, struct peer*);

#endif