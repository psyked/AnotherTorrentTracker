#include "handle_requests.h"


void sendInfoTorrent(int cfd, struct peer *listPeers)
{  // bencode the list of peers and send it to the client

}
void addToList(struct infoTorrent info, struct peer *listPeers)
{  // find the first place available in the list then put the informations in
    for (int i = 0; i < MAX_PEERS; i++)
    {
        if(strcmp(listPeers[i].id, "") == 0)
        {
            listPeers[i].id = info.id;
            listPeers[i].ip = info.ip;
            listPeers[i].port = info.port;
            break;
        }    
    }
}
void delFromList(char *id, struct peer *listPeers)
{  // search the peer id in the list then replace it's id with "" to indicate the place as free
    for (int i = 0; i < MAX_PEERS; i++)
    {
        if(strcmp(listPeers[i].id, id) == 0)
        {
            listPeers[i].id = "";
        }
    }
}
struct infoTorrent parseInfoTorrent(char *strIn, int len)
{
    struct infoTorrent info;
    char *str = strdup(strIn);
    str = strsep(&str, " ");  // get rid of the protocol part
    char *token;
    while(token = strsep(&str, "&"))
    {  // parse informations
        printf("%s\n", token);
        char *cmp = strsep(&token, "=");
        if(strcmp(cmp, "info_hash")==0)
        {
            //printf("info : %s\n", token);
            info.hash = token;
            continue;
        }
        if(strcmp(cmp, "peer_id")==0)
        {
            //printf("id : %s\n", token);
            info.id = token;
            continue;   
        }
        if(strcmp(cmp, "port")==0)
        {
            //printf("port : %s\n", token);
            info.port = atoi(token);
            continue;   
        }
        if(strcmp(cmp, "key")==0)
        {
            //printf("key : %s\n", token);
            info.key = atoi(token);
            continue;   
        }
        if(strcmp(cmp, "uploaded")==0)
        {
            //printf("up : %s\n", token);
            info.uploaded = atoi(token);
            continue;   
        }
        if(strcmp(cmp, "downloaded")==0)
        {
            //printf("down : %s\n", token);
            info.downloaded = atoi(token);
            continue;   
        }
        if(strcmp(cmp, "left")==0)
        {
            //printf("left : %s\n", token);
            info.left = atoi(token);
            continue;   
        }
        if(strcmp(cmp, "compact")==0)
        {
            //printf("comp : %s\n", token);
            info.compact = atoi(token);
            continue;   
        }
        if(strcmp(cmp, "event")==0)
        {
            //printf("ev : %s\n", token);
            info.event = token;
            continue;
        }
    }        
    free(str);
    return info;
}

void handleRequestHTML(int cfd, int currentClients, int totalClients, int totalDownloaded)
{
    char buf[BUFFER_SIZE];
    char filename[BUFFER_SIZE] = {0};
    int nread;
    while ((nread = readLine(cfd, buf, BUFFER_SIZE)) > 0)
    {
        if (strcmp(buf, "\r\n") == 0)
            break;

        if (strstr(buf, "GET"))
        {
            char *ptr = strstr(buf, " HTTP/");
            *ptr = '\0';
            strcpy(filename, buf + 4);
        }
    }
    if (strlen(filename) > 0)
    {
        if (strcmp(filename, "/index.html") == 0 || strcmp(filename, "/") == 0)
        {
            printf("Sending index.html \n");
            sendWebPage(cfd, currentClients, totalClients, totalDownloaded);
        }
        else
        {
            printf("Someone is lost [404]\n");
            sendString(cfd, "HTTP/1.1 404 NOT FOUND\r\n");
            sendString(cfd, "Server: TrackerServer\r\n\r\n");
            sendString(cfd, "<html><head><title>404 Not Found</title></head>");
            sendString(cfd, "<body><p>The page ");
            sendString(cfd, filename);
            sendString(cfd, " doesn't exist, try /index.html</p>"
            "<p style='position: absolute; bottom: 0;right: 0'>For more informations, make sure to "
            "<a href='https://gitlab.com/psyked/AnotherTorrentTracker'>check the git </a> of this project.</p></body></html>");
        }
    }
    else
    {
        printf("Someone is really lost, FD : %d\n", cfd);
        return;
        sendString(cfd, "<html><head><title>Bad request</title></head>");
        sendString(cfd, "<body>Bad filename requested, try /index.html</body></html>");
    }
    sendString(cfd, "\r\n");

    if (nread == -1)
        errExit("Error from read()");
}

void handleRequestTorrent(int cfd, char *clientIP, int *totalDownload, struct peer *listPeers)
{
    struct infoTorrent info;
    clientIP++;  // get rid of the '('
    clientIP = strsep(&clientIP, ",");  // just take the ip
    info.ip = clientIP;
    char buf[BUFFER_SIZE];
    int nread;
    while ((nread = readLine(cfd, buf, BUFFER_SIZE)) > 0)
    {
        printf("[%d] %s", nread, buf);
        // parse if the line read is a GET request
        if(buf[0]=='G' && buf[1] == 'E' && buf[2] == 'T')
            info = parseInfoTorrent(buf+6, nread-6);
        if (strcmp(buf, "\r\n") == 0);
            break;
    }
    if (nread == -1)
        errExit("Error from read");

    if(strcmp(info.hash, HASH_FILE) == 0)
    {
        if(strcmp(info.event, "started") == 0)
        {
            sendInfoTorrent(cfd, listPeers);
            addToList(info, listPeers);
        }
        if(strcmp(info.event, "stopped") == 0)
        {
            delFromList(info.id, listPeers);
        }
        if(strcmp(info.event, "completed") == 0)
            *totalDownload = *totalDownload + 1;
    }
    sendString(cfd, "\r\n");
}
