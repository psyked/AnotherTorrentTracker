#ifndef TRACKER_H
#define TRACKER_H

#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <sys/epoll.h>
#include <stdlib.h>
#include <string.h>

#include "inet_socket.h"
#include "utility.h"
#include "handle_requests.h"

#define BACKLOG 5
#define EPOLL_SIZE 10
#define MAX_EVENTS 10
#define BUFFER_SIZE 1024
#define MAX_PEERS 20 
// number of users' connexions since the start of the server in ASDCII decimal
#define MAX_USERS_BUFFER_STRING 4 

#define NAME_FILE "torrent"
#define HASH_FILE "%82%97%B0%C9%A98%87%3Fe%FC%E3%7F%81%89%87%A7w%9B%AB%C8"

int makeNonBlock(int);

#endif