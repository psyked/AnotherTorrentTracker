#include "tracker4.h"


int makeNonBlock(int fd)
{
    return fcntl(fd, F_SETFL, O_NONBLOCK);
}

int main()
{
    struct peer listPeers[MAX_PEERS];
    for (int i = 0; i < MAX_PEERS; i++)
    {
        listPeers[i].id = ""; 
    }
    // variables to display on web page
    int totalClients = 0;
    int currentClients = 0;
    int totalDownloaded = 0;

    int torrentServerFd = inetListen("55555", BACKLOG, NULL);
    makeNonBlock(torrentServerFd);
    int webServerFd = inetListen("8080", BACKLOG, NULL);
    makeNonBlock(webServerFd);
    if (torrentServerFd == -1)
    {
        errExit("inetListen 55555");
    }
    if (webServerFd == -1)
    {
        errExit("inetListen 8080");
    }

    int epfd;
    epfd = epoll_create(EPOLL_SIZE);
    if (epfd == -1)
        errExit("epoll_create torrent");

    struct epoll_event ev = {.events = EPOLLIN};
    struct epoll_event evH = {.events = EPOLLIN};

    ev.data.fd = torrentServerFd;
    if (epoll_ctl(epfd, EPOLL_CTL_ADD, torrentServerFd, &ev) == -1)
        errExit("epoll_ctl torrent");
    evH.data.fd = webServerFd;
    if (epoll_ctl(epfd, EPOLL_CTL_ADD, webServerFd, &evH) == -1)
    {
        errExit("epoll_ctl html");
    }

    ev.data.fd = STDIN_FILENO;
    if (epoll_ctl(epfd, EPOLL_CTL_ADD, STDIN_FILENO, &ev) == -1)
        errExit("epoll_ctl");

    struct epoll_event eventList[MAX_EVENTS];
    char buffer[BUFFER_SIZE + 1];
    char x = 0;

    while (x != 'q')
    {
        // retreive events of the epoll "binded" on port 55555, 8080 and STDIN_FILENO
        int nbFdReady = epoll_wait(epfd, eventList, MAX_EVENTS, -1);
        if (nbFdReady == -1)
        {
            if (errno == EINTR)
                continue;
            else
                errExit("epoll torrent");
        }
        for (int i = 0; i < nbFdReady; ++i)
        {
            int fd = eventList[i].data.fd;
            if (fd == STDIN_FILENO)
            {  // event on standard input
                if ((read(STDIN_FILENO, &x, 1) == 1) && (x == 'q'))
                    break;
            }
            else if (fd == torrentServerFd)
            {  // event on 55555 : new connection
                struct sockaddr_in client_addr;
                char clientAddrString[INET6_ADDRSTRLEN];
                socklen_t clientAddrStringLength = sizeof(struct sockaddr_in);
                int clientFd = accept(torrentServerFd, (struct sockaddr *)&client_addr, &clientAddrStringLength);
                inetAddressStr((struct sockaddr *)&client_addr, clientAddrStringLength,
                               clientAddrString, INET6_ADDRSTRLEN);

                if (clientFd == -1 && errno != EINTR)
                    errExit("accept torrent");

                if (clientFd != -1)
                {
                    printf("\nAccept a new connection on 55555 from %s\n", clientAddrString);
                    totalClients++;
                    currentClients++;
                    ev.data.fd = clientFd;
                    if (epoll_ctl(epfd, EPOLL_CTL_ADD, clientFd, &ev) == -1)
                        close(clientFd);
                    handleRequestTorrent(clientFd, clientAddrString, &totalDownloaded, listPeers);
                    // close the fd to let the client display the page
                    close(clientFd);
                    printf("Connection closed by server\n");
                    currentClients--;
                }
            }
            else if (fd == webServerFd)
            {  // event on 8080 : new connection
                struct sockaddr_in client_addr;
                char clientAddrString[INET6_ADDRSTRLEN];
                socklen_t clientAddrStringLength = sizeof(struct sockaddr_in);
                int clientFd = accept(webServerFd, (struct sockaddr *)&client_addr, &clientAddrStringLength);
                inetAddressStr((struct sockaddr *)&client_addr, clientAddrStringLength,
                               clientAddrString, INET6_ADDRSTRLEN);
                if (clientFd == -1 && errno != EINTR)
                {
                    errExit("accept html");
                }
                // There is a new connection
                printf("\nAccept a new connection on 8080 from %s\n", clientAddrString);
                totalClients++;
                currentClients++;
                ev.data.fd = clientFd;
                if (epoll_ctl(epfd, EPOLL_CTL_ADD, clientFd, &ev) == -1)
                    close(clientFd);
                // sent the web page
                handleRequestHTML(clientFd, currentClients, totalClients, totalDownloaded);
                // close the fd to let the client display the page
                close(clientFd);
                printf("Connection close by server\n");
                currentClients--;
            }
            else
            {  // event triggered from a FD of a connected client
                int nread = read(fd, buffer, BUFFER_SIZE);

                if (fd == STDIN_FILENO)
                {  // standard input
                    if ((read(STDIN_FILENO, &x, 1) == 1) && (x == 'q'))
                        break;
                }
                else if (nread < 0 && errno != EINTR)
                {
                    errExit("read fd client");
                }
                else if (nread == 0)
                {  // a client left
                    printf("Connection closed by client\n");
                    currentClients--;
                    close(fd);
                    continue;
                }
                buffer[nread] = 0;
                printf("Receive from file descriptor (%d) : %s\n", fd, buffer);
            }
        }
    }
    printf("closing server\n");
    close(torrentServerFd);
    close(webServerFd);
    return EXIT_SUCCESS;
}