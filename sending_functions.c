#include "sending_functions.h"


void sendData(int fd, const char *buf, size_t len)
{
    ssize_t n;
    size_t pos = 0;
    const char *ptr = buf;

    while (len > pos)
    {
        switch (n = write(fd, ptr + pos, len - pos))
        {
        case 0:
            _exit(0);
        /* NOTREACHED */
        case -1:
            if (errno != EINTR && errno != EAGAIN)
                perror("send_data");
            break;
        default:
            pos += n;
        }
    }
}
void sendString(int fd, const char *msg)
{
    sendData(fd, msg, strlen(msg));
}

void sendWebPage(int cfd, int currentClients, int totalClients, int totalDownloaded)
{
    sendString(cfd, "HTTP/1.1 200 OK\r\n");
    sendString(cfd, "Server: TrackerServer\r\n\r\n");
    sendString(cfd, "<!DOCTYPE html"
                     "PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN'"
                     "'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'>"
                     "<html xmlns='http://www.w3.org/1999/xhtml'>"
                     "<head>"
                     "<title>Tracker information</title>"
                     "</head>"
                     "<body><h1>Welcome on another torrent tracker server</h1><p>"
                     "Opened Connexion");
    char *s = (currentClients < 2)?"\0":"s\0";
    sendString(cfd, s);
    sendString(cfd, " : <b>");
    char buff[MAX_USERS_BUFFER_STRING];
    sprintf(buff, "%d", currentClients);
    sendString(cfd, buff);
    sendString(cfd, "</b><br/> Connection");
    s=(totalClients < 2)?"\0":"s\0";
    sendString(cfd, s);
    sendString(cfd," since the beginning of the server : <b>");
    sprintf(buff, "%d", totalClients);
    sendString(cfd, buff); 
    sendString(cfd, "</b><br/>Number of donwload");
    s=(totalDownloaded < 2)?"\0":"s\0";
    sendString(cfd, s);
    sendString(cfd," since the beginning of the server : <b>");
    sprintf(buff, "%d", totalDownloaded);
    sendString(cfd, buff); 
    sendString(cfd, "</b></p><p>Hash of the file : <b>");
    sendString(cfd, HASH_FILE);
    sendString(cfd, "</b>""<br/>Name of the file : <b>");
    sendString(cfd, NAME_FILE);
    printf("%s", NAME_FILE);
    sendString(cfd, "</b></p><p style='position: absolute; bottom: 0;right: 0'>For more informations,"
        " make sure to <a href='https://gitlab.com/psyked/AnotherTorrentTracker'>check the git </a> of this project"
        ".</p></body></html>");
}
