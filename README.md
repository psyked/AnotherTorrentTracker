<h1>Another Torrent Tracker</h1>
<p><small><i>
    By BELLETOILE Benjamin & FRANCOIS Edouard
</i></small></p>
<br/>
<p>
    This 2 weeks projet is a torrent tracker for GNU/Linux written in C/C++.
</p>
<p>
    Convention used : <a href= https://msdn.microsoft.com/en-us/library/aa260844(VS.60).aspx>Microsoft standard for .NET</a><br/>
    Tested on : WSL on windows 10 Technical Preview build 15046 with Ubuntu16.10-Xenial (gcc v5.4) & on Kali-rolling (gcc v6.4)<br/>
    How to use : clone this repo, launch the <b>make</b> command from the folder then execute the binary file tracker4.<br/>
    <br/>
    The tracker2.c is implemented directly in tracker4 which combine all other trackers and adapt its architecture to comply with them.<br/>
    Fonctionnalities : the tracker4 listen and display informations of incomming clients on ports 55555 and 8080 then add the torrent client in a list (if needed) or display a webpage.<br/>
    TODO : the tracker4 don't send yet the "bencoded" list of peers.<br/>
</p>
